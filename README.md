# FlowEngine Injector #

FlowEngine Injector is part of the FlowEngine project. Injector is Injector is intended to work as the integrator between modules that should be used together. It acts as a simple IoC container with an easy to extend configuration to adapt it to your needs. After the IoC container is called, you just inject your objects as desired. What we do is set up our BaseIoCContainer with any given Configurators:


```
#!java

// testContainer is a reference to BaseIoCContainer instance

testContainer.registerConfigurator(new Configurator() {
	public void configure(IoCContainer container) {
		container.registerSingleton(MyServiceInterface.class, MyService.class);
	}
}, BaseIoCContainer.IoCConsts.MAX_ORDER);
testContainer.registerConfigurator(new Configurator() {
	public void configure(IoCContainer container) {
		container.registerSingleton(MyServiceInterface2.class, MyService2.class);
	}
}, BaseIoCContainer.IoCConsts.MIN_ORDER);
testContainer.runConfiguration();

```

And then we use it like this:


```
#!java
MyInjectableClass target = new MyInjectableClass();
testContainer.inject(target);
```

While it is not necessary to use multiple configuration to inject our classes, grouping our dependencies into modules should be a good idea. Also note that configurations may run on different order, as they are associated with an execution priority. Configurators with higher priority will be invoked first.

Injector will then scan target public attributes and try to inject them in an optimistic way. It will not detect unresolved dependencies for target, as it doesn't keep track of them. 

The important part here is the Configurator, which is the interface used to extend and simplify IoC Container configuration. This way, you should only define a Configurator for any given module and register it on application start in IoCContainer and run its configuration.

See [https://packagecloud.io/flowengine/injector](Link URL) for installation steps