package FlowEngine.injection;

import flowengine.injection.BaseIoCContainer;
import flowengine.injection.BaseIoCInjector;
import flowengine.injection.Configurator;
import flowengine.injection.IoCContainer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;


public class IoCContainerTest
    extends TestCase {

    public IoCContainerTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
        testContainer = new BaseIoCContainer(new BaseIoCInjector());
    }

    private IoCContainer testContainer;

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(IoCContainerTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testSingleInjectionSingletonMethod() {
        testContainer.registerSingleton(TestInjectionInterface.class, new TestImplementor());
        TestClass target = new TestClass();
        testContainer.inject(target);
        assertNotNull(target.testInjection);
    }

    public void testSingleInjectionTransientMethod() {
        testContainer.registerTransient(TestInjectionInterface.class, TestImplementor.class);
        testContainer.runConfiguration();
        TestClass target = new TestClass();
        testContainer.inject(target);
        assertNotNull(target.testInjection);
    }

    public void testTwoImplementorTwoInterfacesInjectionMethod() {
        TestImplementor implementor = new TestImplementor();
        TestImplementor2 implementor2 = new TestImplementor2();
        testContainer.registerSingleton(TestInjectionInterface.class, implementor);
        testContainer.registerSingleton(TestInjectionInterface2.class, implementor2);
        testContainer.runConfiguration();
        TestClass target = new TestClass();
        testContainer.inject(target);
        assertNotNull(target.testInjection);
        assertNotNull(target.testInjection2);
    }


    public void testOneImplementorTwoInterfacesInjectionMethod() {
        TestSingle implementor = new TestSingle();
        testContainer.registerSingleton(TestInjectionInterface.class, implementor);
        testContainer.registerSingleton(TestInjectionInterface2.class, implementor);
        testContainer.runConfiguration();
        TestClass target = new TestClass();
        testContainer.inject(target);
        assertNotNull(target.testInjection);
        assertNotNull(target.testInjection2);
    }


    public void testCircularResolution() {
        CircularTestImplementor implementor = new CircularTestImplementor();
        CircularTestImplementor2 implementor2 = new CircularTestImplementor2();
        testContainer.registerSingleton(TestInjectionInterface.class, implementor);
        testContainer.registerSingleton(TestInjectionInterface2.class, implementor2);
        testContainer.runConfiguration();
        TestClass target = new TestClass();
        testContainer.inject(target);
        assertNotNull(target.testInjection);
        assertNotNull(implementor.implementor);
        assertNotNull(target.testInjection2);
        assertNotNull(implementor2.implementor);
    }

    public void testCircularResolutionWithConfiguration() {
        testContainer.registerConfigurator(new Configurator() {
            public void configure(IoCContainer container) {
                CircularTestImplementor implementor = new CircularTestImplementor();
                CircularTestImplementor2 implementor2 = new CircularTestImplementor2();
                container.registerSingleton(TestInjectionInterface.class, implementor);
                container.registerSingleton(TestInjectionInterface2.class, implementor2);
            }
        });
        testContainer.runConfiguration();
        TestClass target = new TestClass();
        testContainer.inject(target);
        assertNotNull(target.testInjection);
        assertNotNull(((CircularTestImplementor) target.testInjection).implementor);
        assertNotNull(target.testInjection2);
        assertNotNull(((CircularTestImplementor) target.testInjection).implementor);
    }


    public void testConfigurationWithFirstExecutesBeforeLast() {
        final ArrayList<Integer> executionOrder = new ArrayList();
        testContainer.registerConfigurator(new Configurator() {
            public void configure(IoCContainer container) {
                executionOrder.add(BaseIoCContainer.IoCConsts.MAX_ORDER);
            }
        }, BaseIoCContainer.IoCConsts.MAX_ORDER);
        testContainer.registerConfigurator(new Configurator() {
            public void configure(IoCContainer container) {
                executionOrder.add(BaseIoCContainer.IoCConsts.MIN_ORDER);
            }
        }, BaseIoCContainer.IoCConsts.MIN_ORDER);
        testContainer.runConfiguration();
        assertEquals(2, executionOrder.size());
        assertEquals(new Integer(BaseIoCContainer.IoCConsts.MAX_ORDER), executionOrder.get(0));
        assertEquals(new Integer(BaseIoCContainer.IoCConsts.MIN_ORDER), executionOrder.get(1));
    }


}
