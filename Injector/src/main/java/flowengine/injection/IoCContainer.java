package flowengine.injection;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Created by Ignacio on 11/07/2015.
 */
public interface IoCContainer {

    void registerConfigurator(Configurator configurator, int order);

    void registerConfigurator(Configurator configurator);

    void runConfiguration();

    void registerSingleton(Class<?> representingInterface, Object object);

    void registerTransient(Class<?> representingInterface, Class<?> implementingClass);

    <T> T getImplementor(Class<T> representingInterface);

    boolean hasImplementor(Field field);

    Collection<Object> getAllSingletons();

    void resolveInjectedSingletonDependencies();

    void inject(Object target);
}
