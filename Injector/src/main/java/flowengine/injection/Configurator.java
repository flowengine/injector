package flowengine.injection;

/**
 * Created by Ignacio on 19/08/2016.
 */
public interface Configurator {
    void configure(IoCContainer container);
}
