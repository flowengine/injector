package flowengine.injection;

/**
 * Created by Ignacio on 19/08/2016.
 */
public class SingletonConfiguration implements Configurator {

    public void configure(IoCContainer container) {
        container.resolveInjectedSingletonDependencies();
    }
}
