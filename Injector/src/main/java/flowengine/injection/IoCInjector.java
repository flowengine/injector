package flowengine.injection;

/**
 * Created by Ignacio on 11/07/2015.
 */
public interface IoCInjector {

    void inject(Object target, IoCContainer container);

    void resolveInjectedSingletonDependencies(IoCContainer container);
}
