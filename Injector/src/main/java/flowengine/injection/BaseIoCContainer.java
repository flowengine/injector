package flowengine.injection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Created by Ignacio on 11/07/2015.
 */
public class BaseIoCContainer implements IoCContainer {

    public class IoCConsts {
        public static final int MIN_ORDER = Integer.MIN_VALUE + 1;
        public static final int DEFAULT_ORDER = 0;
        public static final int MAX_ORDER = Integer.MAX_VALUE - 1;
    }

    private static BaseIoCContainer instance;

    private Map<Class<?>, Object> registeredSingletons;

    private Map<Class<?>, Class<?>> registeredTransients;

    private PriorityQueue<ConfiguratorPriorityQueueElement> configurators;

    private IoCInjector injector;

    public BaseIoCContainer(IoCInjector injector) {
        registeredSingletons = new HashMap();
        registeredTransients = new HashMap();
        this.injector = injector;
        configurators = new PriorityQueue();
        configurators.add(new ConfiguratorPriorityQueueElement(Integer.MAX_VALUE, new SingletonConfiguration()));
    }

    public static BaseIoCContainer getInstance() {
        if (instance == null) {
            instance = new BaseIoCContainer(new BaseIoCInjector());
        }
        return instance;
    }

    public void runConfiguration() {
        while (!configurators.isEmpty()) {
            configurators.poll().configurator.configure(this);
        }
    }

    public void registerConfigurator(Configurator configurator) {
        registerConfigurator(configurator, IoCConsts.DEFAULT_ORDER);
    }

    public void registerConfigurator(Configurator configurator, int order) {
        if (order > IoCConsts.MAX_ORDER || order < IoCConsts.MIN_ORDER) {
            throw new RuntimeException("Order must be between MIN_ORDER and MAX_ORDER");
        }

        configurators.add(new ConfiguratorPriorityQueueElement(order, configurator));
    }

    public void registerSingleton(Class<?> representingInterface, Object object) {
        if (object == null)
            throw new RuntimeException("Cannot register null as implementor of " + representingInterface.toString());
        if (registeredSingletons.containsKey(representingInterface))
            throw new RuntimeException("Cannot register " + object.getClass().toString() + " as implementor of " + representingInterface.toString() + " because there is already an implementor for such interface");
        if (!implementsInterface(representingInterface, object))
            throw new RuntimeException("Object of class " + object.getClass().toString() + " cannot be registered as an implementor for " + representingInterface + " interface");
        registeredSingletons.put(representingInterface, object);
    }

    public void registerTransient(Class<?> representingInterface, Class<?> implementorClass) {
        if (registeredTransients.containsKey(representingInterface))
            throw new RuntimeException("Cannot register " + implementorClass.toString() + " as implementor of " + representingInterface.toString() + " because there is already an implementor for such interface");
        if (!implementsInterface(representingInterface, implementorClass))
            throw new RuntimeException("Object of class " + implementorClass.toString() + " cannot be registered as an implementor for " + representingInterface + " interface");
        if (!hasZeroArgumentConstructorDefined(implementorClass))
            throw new RuntimeException("Object of class " + implementorClass.toString() + " cannot be registered as an implementor for " + representingInterface + " interface because it has no zero argument constructor");
        registeredTransients.put(representingInterface, implementorClass);
    }

    private boolean hasZeroArgumentConstructorDefined(Class<?> implementorClass) {
        Constructor<?>[] constructors = implementorClass.getConstructors();
        if (constructors == null || constructors.length == 0)
            return false;
        for (Constructor<?> constructor : constructors) {
            if (constructor.getParameterTypes().length == 0) {
                return true;
            }
        }
        return false;
    }

    private boolean implementsInterface(Class<?> checkInterface, Class<?> implementorClass) {
        for (Class<?> currentInterface : implementorClass.getInterfaces()) {
            if (currentInterface == checkInterface) {
                return true;
            }
        }
        return false;
    }

    private boolean implementsInterface(Class<?> checkInterface, Object object) {
        return object != null && implementsInterface(checkInterface, object.getClass());
    }

    public <T> T getImplementor(Class<T> representingInterface) {
        if (!registeredSingletons.containsKey(representingInterface) && !registeredTransients.containsKey(representingInterface))
            throw new RuntimeException("Component  " + representingInterface.toString() + " has no registered implementor");
        if (registeredSingletons.containsKey(representingInterface))
            return (T) registeredSingletons.get(representingInterface);
        return buildTransientImplementor(registeredTransients.get(representingInterface));
    }

    private <T> T buildTransientImplementor(Class<?> aClass) {
        try {
            return (T) aClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean hasImplementor(Field field) {
        return registeredSingletons.containsKey(field.getType()) || registeredTransients.containsKey(field.getType());
    }

    public Collection<Object> getAllSingletons() {
        return registeredSingletons.values();
    }

    public void resolveInjectedSingletonDependencies() {
        injector.resolveInjectedSingletonDependencies(this);
    }

    public void inject(Object target) {
        injector.inject(target, this);
    }

    private class ConfiguratorPriorityQueueElement implements Comparable<ConfiguratorPriorityQueueElement> {

        private final Configurator configurator;
        private final int priority;

        public ConfiguratorPriorityQueueElement(int priority, Configurator configurator) {
            this.priority = priority;
            this.configurator = configurator;
        }

        public int compareTo(ConfiguratorPriorityQueueElement o) {
            return this.priority - o.priority;
        }
    }

}