package flowengine.injection;

import java.lang.reflect.Field;

/**
 * Created by Ignacio on 11/07/2015.
 */
public class BaseIoCInjector implements IoCInjector {

    public void inject(Object target, IoCContainer container) {
        Field[] fields = target.getClass().getFields();
        for (Field field : fields) {
            if (container.hasImplementor(field)) {
                try {
                    field.set(target, container.getImplementor(field.getType()));
                } catch (IllegalAccessException e) {

                }
            }
        }
    }

    public void resolveInjectedSingletonDependencies(IoCContainer container) {
        for (Object target : container.getAllSingletons()) {
            inject(target, container);
        }
    }
}
